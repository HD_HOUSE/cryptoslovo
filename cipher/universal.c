#include "data.h"
#include "inline.h"
#include "alignas.h"
#include "restrict.h"
#include "libgost15.h"
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define ALIGN(x) __attribute__((__aligned__(x)))

static inline bool
isAligned(
    void const *ptr,
    size_t alignment)
{
    return !(((uintptr_t)ptr) % alignment);
}

/*
 * Обработка
 */

static inline void
substituteBytes(
    uint8_t *restrict block,
    uint8_t const *restrict sbox)
{
    assert(block);
    assert(sbox);

    for (size_t i = 0; i < BlockLengthInBytes; ++i)
        block[i] = sbox[block[i]];
}

static inline void
directSubstituteBytes(
    uint8_t *block)
{
    substituteBytes(block, lg15_directPi);
}

static inline void
inverseSubstituteBytes(
    uint8_t *block)
{
    substituteBytes(block, lg15_inversePi);
}

static inline uint8_t *
getAt(
    uint8_t const *table,
    unsigned index)
{
    assert(table);
    assert(index < 0x1000);

    return (uint8_t *)&table[index * BlockLengthInBytes];
}

static inline void
xorBlocks(
    void *restrict dst,
    void const *restrict src)
{
    assert(dst);
    assert(isAligned(dst, 8));
    assert(src);
    assert(isAligned(src, 8));

    uint64_t
        *dst_ = dst;
    uint64_t const
        *src_ = src;
    size_t const
        size_ = BlockLengthInBytes / sizeof(uint64_t);

    for (size_t i = 0; i < size_; ++i)
        dst_[i] ^= src_[i];

    return;
}

static inline void
copyBlock(
    void *restrict dst,
    void const *restrict src)
{
    assert(dst);
    assert(isAligned(dst, 8));
    assert(src);
    assert(isAligned(src, 8));

    uint64_t
        *dst_ = dst;
    uint64_t const
        *src_ = src;
    size_t const
        size_ = BlockLengthInBytes / sizeof(uint64_t);

    for (size_t i = 0; i < size_; ++i)
        dst_[i] = src_[i];

    return;
}

static inline void
transform(
    uint8_t *restrict block,
    uint8_t const *restrict table)
{
    assert(block);
    assert(isAligned(block, 8));
    assert(table);

    alignas(8) uint8_t
        buffer[BlockLengthInBytes] = {0};

    xorBlocks(buffer, getAt(table, block[0x0] + 0x000));
    xorBlocks(buffer, getAt(table, block[0x1] + 0x100));
    xorBlocks(buffer, getAt(table, block[0x2] + 0x200));
    xorBlocks(buffer, getAt(table, block[0x3] + 0x300));
    xorBlocks(buffer, getAt(table, block[0x4] + 0x400));
    xorBlocks(buffer, getAt(table, block[0x5] + 0x500));
    xorBlocks(buffer, getAt(table, block[0x6] + 0x600));
    xorBlocks(buffer, getAt(table, block[0x7] + 0x700));
    xorBlocks(buffer, getAt(table, block[0x8] + 0x800));
    xorBlocks(buffer, getAt(table, block[0x9] + 0x900));
    xorBlocks(buffer, getAt(table, block[0xa] + 0xa00));
    xorBlocks(buffer, getAt(table, block[0xb] + 0xb00));
    xorBlocks(buffer, getAt(table, block[0xc] + 0xc00));
    xorBlocks(buffer, getAt(table, block[0xd] + 0xd00));
    xorBlocks(buffer, getAt(table, block[0xe] + 0xe00));
    xorBlocks(buffer, getAt(table, block[0xf] + 0xf00));

    copyBlock(block, buffer);
}

static inline void
directTransform(
    uint8_t *block)
{
    transform(block, lg15_directTable);
}

static inline void
inverseTransform(
    uint8_t *block)
{
    transform(block, lg15_inverseTable);
}


/// @brief Фукция шифрования
/// @param roundKeys раудовые кллючи шифрования
/// @param blocks блоки для шифрования (у нас всегда 1 блок, 16 байт)
/// @param numberOfBlocks колличество блоков (у нас всегда 1)
extern void
lg15_encryptBlocks(
    uint8_t const *restrict roundKeys,
    uint8_t *restrict blocks,
    size_t numberOfBlocks)
{
    assert(roundKeys);
    assert(isAligned(roundKeys, 8));
    assert(blocks);
    assert(isAligned(blocks, 8));

    for (size_t i = 0; i < numberOfBlocks; ++i)
    {
        uint8_t *restrict block = getAt(blocks, i);
        unsigned
            round = 0;

        xorBlocks(block, getAt(roundKeys, round));
        ++round;

        for (; round < NumberOfRounds; ++round)
        {
            directTransform(blocks);
            xorBlocks(blocks, getAt(roundKeys, round));
        }
    }

    return;
}

/// @brief функция расшифровки
/// @param roundKeys раундовые ключи
/// @param blocks блоки для расшифровки (у нас всегда 1 блок, 16 байт)
/// @param numberOfBlocks количество блоков (у нас всегда 1)
extern void
lg15_decryptBlocks(
    uint8_t const *restrict roundKeys,
    uint8_t *restrict blocks,
    size_t numberOfBlocks)
{
    assert(roundKeys);
    assert(isAligned(roundKeys, 8));
    assert(blocks);
    assert(isAligned(blocks, 8));

    for (size_t i = 0; i < numberOfBlocks; ++i)
    {
        uint8_t *restrict block = getAt(blocks, i);
        unsigned
            round = NumberOfRounds - 1;

        xorBlocks(block, getAt(roundKeys, round));
        --round;

        directSubstituteBytes(block);
        inverseTransform(block);
        inverseTransform(block);
        xorBlocks(block, getAt(roundKeys, round));
        --round;

        for (; round > 0; --round)
        {
            inverseTransform(block);
            xorBlocks(block, getAt(roundKeys, round));
        }

        inverseSubstituteBytes(block);
        xorBlocks(block, getAt(roundKeys, round));
    }

    return;
}

/*
 * Составление раундовых ключей
 */

static inline uint8_t *
constantAt(
    unsigned index)
{
    return getAt(lg15_constants, index);
}

static inline void
feistelRoundWithoutSwap(
    uint8_t *left,
    uint8_t const *right,
    unsigned constantIndex)
{
    alignas(8) uint8_t
        buffer[BlockLengthInBytes];

    copyBlock(buffer, right);
    xorBlocks(buffer, constantAt(constantIndex));
    directTransform(buffer);
    xorBlocks(left, buffer);
}


/// @brief функция развертки ключей шифрования
/// @param roundKeys указатель на область памяти в которую будут записаны раундовые ключи (160 байт)
/// @param key ключ (32 байта)
extern void
lg15_scheduleEncryptionRoundKeys(
    uint8_t *restrict roundKeys,
    uint8_t const *restrict key)
{
    assert(roundKeys);
    assert(isAligned(roundKeys, 8));
    assert(key);
    assert(isAligned(key, 8));

    unsigned
        i = 0;

    copyBlock(getAt(roundKeys, 0), getAt(key, 0));
    copyBlock(getAt(roundKeys, 1), getAt(key, 1));
    i += 2;

    for (unsigned constantIndex = 0; i != NumberOfRounds; i += 2)
    {
        uint8_t
            *left = getAt(roundKeys, i),
            *right = getAt(roundKeys, i + 1);

        copyBlock(left, getAt(roundKeys, i - 2));
        copyBlock(right, getAt(roundKeys, i - 1));

        for (size_t round = 0; round < NumberOfRoundsInKeySchedule; round += 2)
        {
            feistelRoundWithoutSwap(right, left, constantIndex++);
            feistelRoundWithoutSwap(left, right, constantIndex++);
        }
    }

    return;
}


/// @brief функция развертки ключей расшифровки
/// @param roundKeys указатель на область памяти в которую будут записаны раундовые ключи (160 байт)
/// @param key ключ (32 байта)
extern void
lg15_scheduleDecryptionRoundKeys(
    uint8_t *restrict roundKeys,
    uint8_t const *restrict key)
{
    lg15_scheduleEncryptionRoundKeys(roundKeys, key);

    for (unsigned i = 1; i <= NumberOfRounds - 2; ++i)
    {
        directSubstituteBytes(getAt(roundKeys, i));
        inverseTransform(getAt(roundKeys, i));
    }
}

/// @brief функция выделения памяти
/// @param size количество запрашиваемой памяти (в байтах)
/// @return указатель на выделленную память
static void *
memalloc(const size_t size)
{
    void *p;

    posix_memalign(&p, (size_t)64, size);

    return p;
}

static void
reverse_order(unsigned char *in, size_t len)
{
    unsigned char c;
    unsigned int i, j;

    for (i = 0, j = (unsigned int)len - 1; i < j; i++, j--)
    {
        c = in[i];
        in[i] = in[j];
        in[j] = c;
    }
}

static void
convert_to_hex(unsigned char *in, unsigned char *out, size_t len,
               const unsigned int eflag)
{
    unsigned int i;
    char ch[3];

    if (len > 64)
        len = 64;

    memset(out, 0, 129);

    // eflag устанавливается при запросе вывода little-endian 
    if (eflag)
        reverse_order(in, len);

    for (i = 0; i < len; i++)
    {
        sprintf(ch, "%02x", (unsigned char)in[i]);
        memcpy(&out[i * 2], ch, 2);
    }
}


/// @brief функция шифрования в режиме ECB
/// @param message открытый текст
/// @param key ключ шифрования (32 байта)
/// @param messageLen длина открытого текста
void __attribute__((visibility("default"))) __attribute__((used)) encryptECB(uint8_t *message, uint8_t *key, size_t messageLen)
{
    // выделяем память для хранения раудовых ключей
    uint8_t *roundKeys = memalloc(160);
    
    // создаем раундовые ключи
    lg15_scheduleEncryptionRoundKeys(roundKeys, key);

    // шифруем открытый текст по одному блоку
    for (int i = 0; i < messageLen; i += 16)
    {
        lg15_encryptBlocks(roundKeys, message + i, 1);
    }

    // освобождаем аллоцированную память
    free(roundKeys);
}

/// @brief фукция расшифровки в режиме ECB
/// @param message  шифротекст
/// @param key ключ шифрования (32 байта)
/// @param messageLen длина шифротекста
void __attribute__((visibility("default"))) __attribute__((used)) decryptECB(uint8_t *message, uint8_t *key, size_t messageLen)
{
    // выделям память для хранения раудовых ключей
    uint8_t *roundKeys = memalloc(160);

    // создаем раундовые ключи
    lg15_scheduleDecryptionRoundKeys(roundKeys, key);

    // расшифровываем шифротекст по одному блоку
    for (int i = 0; i < messageLen; i += 16)
    {
        lg15_decryptBlocks(roundKeys, message + i, 1);
    }

    // освобождаем выделенную память
    free(roundKeys);
}

void xorBytes(uint8_t *lhs, uint8_t *rhs, size_t len)
{
    for (size_t i = 0; i < len; ++i)
    {
        lhs[i] ^= (rhs[i]);
    }
}

/// @brief функция шифрования в режиме CBC
/// @param initVector инициализирущий вектор (16 байт)
/// @param message открытый текст
/// @param key ключ шифрования (32 байта)
/// @param messageLen длина открытого текста 
void __attribute__((visibility("default"))) __attribute__((used)) encryptCBC(uint8_t *initVector, uint8_t *message, uint8_t *key, size_t messageLen)
{
    // выделяем память для хранения раундовых ключей
    uint8_t *roundKeys = memalloc(160);

    // создаем раундовые ключи
    lg15_scheduleEncryptionRoundKeys(roundKeys, key);

    // выделяем память для хранения текущего значения гаммы
    uint8_t *currentGamma = memalloc(16);
    // иницилизируем текущее значение гаммы инициализирующим вектором
    memcpy(currentGamma, initVector, 16);

    // шифруем инициализирующий вектор
    lg15_encryptBlocks(roundKeys, initVector, 1);

    for (size_t i = 0; i < messageLen; i += 16)
    {
        // xor'им блок открытого текста с текущей гаммы
        xorBytes(message + i, currentGamma, 16);
        //шифруем то что получилось после xor'а
        lg15_encryptBlocks(roundKeys, message + i, 1);
        // получивышийся блок шифротекста является гаммой для следующего
        memcpy(currentGamma, message + i, 16);
    }

    //особождаем выделенную память
    free(roundKeys);
    free(currentGamma);
}

/// @brief функция расшифровки в режиме CBC
/// @param message шифротекст
/// @param key ключ шифрования (32 байта)
/// @param messageLen длина шифротекста
void __attribute__((visibility("default"))) __attribute__((used)) decryptCBC(uint8_t *message, uint8_t *key, size_t messageLen)
{
    // выделяем память для хранения раундовых ключей
    uint8_t *roundKeys = memalloc(160);

    // создаем раундовые ключи
    lg15_scheduleDecryptionRoundKeys(roundKeys, key);

    // выдлеляем память для текущей и следующей гаммы
    uint8_t *currentGamma = memalloc(16);
    uint8_t *nextGamma = memalloc(16);

    // расшифровываем инициализирующий ветор
    lg15_decryptBlocks(roundKeys, message, 1);
    // инициализируем значение следующей гаммы инициализирующим вектором
    memcpy(nextGamma, message, 16);
    for (size_t i = 16; i < messageLen; i += 16)
    {
        // копируем в текущую гамму значение следующей
        memcpy(currentGamma, nextGamma, 16);
        // следующей гамме присваиваем значение следующего блока шифроткеста
        memcpy(nextGamma, message + i, 16);
        
        // расшифроваваем блок шифротекста
        lg15_decryptBlocks(roundKeys, message + i, 1);

        // xor'им результат с текущей гаммой
        xorBytes(message + i, currentGamma, 16);
    }

    // освобождаем выделенную память
    free(roundKeys);
    free(currentGamma);
    free(nextGamma);
}
