#include <iostream>
#include "cipher_test_data.h"
#include <gtest/gtest.h>

extern "C"
{
    void encryptECB(uint8_t *message, uint8_t *key, size_t messageLen);
    void decryptECB(uint8_t *message, uint8_t *key, size_t messageLen);
    void encryptCBC(uint8_t *initVector, uint8_t *message, uint8_t *key, size_t messageLen);
    void decryptCBC(uint8_t *message, uint8_t *key, size_t messageLen);
}

TEST(CipherTest, EncryptECB1Block)
{
    uint8_t *key = (unsigned char *)&testEncryptECB1BlockKey;
    uint8_t *message = (unsigned char *)&testEncryptECB1BlockInput;
    int size = sizeof(testEncryptECB1BlockInput);
    encryptECB(message, key, size);
    for (int i = 0; i < size; ++i)
    {
        EXPECT_EQ(message[i], testEncryptECB1BlockResult[i]);
    }
}

TEST(CipherTest, EncryptECB2Block)
{
    uint8_t *key = (unsigned char *)&testEncryptECB2BlockKey;
    uint8_t *message = (unsigned char *)&testEncryptECB2BlockInput;
    int size = sizeof(testEncryptECB2BlockInput);
    encryptECB(message, key, size);
    for (int i = 0; i < size; ++i)
    {
        EXPECT_EQ(message[i], testEncryptECB2BlockResult[i]);
    }
}

TEST(CipherTest, DecryptECB1Block)
{
    uint8_t *key = (unsigned char *)&testDecryptECB1BlockKey;
    uint8_t *message = (unsigned char *)&testDecryptECB1BlockInput;
    int size = sizeof(testDecryptECB1BlockInput);
    decryptECB(message, key, size);
    for (int i = 0; i < size; ++i)
    {
        EXPECT_EQ(message[i], testDecryptECB1BlockResult[i]);
    }
}

TEST(CipherTest, DecryptECB2Block)
{
    uint8_t *key = (unsigned char *)&testDecryptECB2BlockKey;
    uint8_t *message = (unsigned char *)&testDecryptECB2BlockInput;
    int size = sizeof(testDecryptECB2BlockInput);
    decryptECB(message, key, size);
    for (int i = 0; i < size; ++i)
    {
        EXPECT_EQ(message[i], testDecryptECB2BlockResult[i]);
    }
}


TEST(CipherTest, EncryptCBC1Block)
{
    uint8_t *initVector = (unsigned char*)&testEncryptCBC1BlockInitVector;
    uint8_t *key = (unsigned char *)&testEncryptCBC1BlockKey;
    uint8_t *message = (unsigned char *)&testEncryptCBC1BlockInput;
    int size = sizeof(testEncryptCBC1BlockInput);
    encryptCBC(initVector,message, key, size);
    for (int i = 0; i < size; ++i)
    {
        EXPECT_EQ(message[i], testEncryptCBC1BlockResult[i]);
    }
}


TEST(CipherTest, EncryptCBC2Block)
{
    uint8_t *initVector = (unsigned char*)&testEncryptCBC2BlockInitVector;
    uint8_t *key = (unsigned char *)&testEncryptCBC2BlockKey;
    uint8_t *message = (unsigned char *)&testEncryptCBC2BlockInput;
    int size = sizeof(testEncryptCBC2BlockInput);
    encryptCBC(initVector,message, key, size);
    for (int i = 0; i < size; ++i)
    {
        EXPECT_EQ(message[i], testEncryptCBC2BlockResult[i]);
    }
}

TEST(CipherTest, DecryptCBC1Block)
{
    uint8_t *key = (unsigned char *)&testDecryptCBC1BlockKey;
    uint8_t *message = (unsigned char *)&testDecryptCBC1BlockInput;
    int size = sizeof(testDecryptCBC1BlockInput);
    decryptCBC(message, key, size);
    for (int i = 0; i < size; ++i)
    {
        EXPECT_EQ(message[i], testDecryptCBC1BlockResult[i]);
    }
}

TEST(CipherTest, DecryptCBC2Block)
{
    uint8_t *key = (unsigned char *)&testDecryptCBC2BlockKey;
    uint8_t *message = (unsigned char *)&testDecryptCBC2BlockInput;
    int size = sizeof(testDecryptCBC2BlockInput);
    decryptCBC(message, key, size);
    for (int i = 0; i < size; ++i)
    {
        EXPECT_EQ(message[i], testDecryptCBC2BlockResult[i]);
    }
}