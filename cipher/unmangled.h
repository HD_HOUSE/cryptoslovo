#pragma once

#include "language.h"

 /* Определяет стандартную спецификацию экспорта для всех публичных деклараций
  * для которых требуется простая привязка C (это используется для большинства, если не для всех экспортируемых символов).
  */
  
#if !defined(_Unmangled)
     #if LANGUAGE_IS_CXX
          #define _Unmangled \
               extern "C"

     #else
          #define _Unmangled \
               extern

     #endif

#endif
