//#pragma once

#include "unmangled.h"
#include <stddef.h>
#include <stdint.h>

enum {
    NumberOfRounds = 10,
    NumberOfRoundsInKeySchedule = 8,

    BlockLengthInBytes = 128 / 8,
    KeyLengthInBytes = 256 / 8,
};

_Unmangled void
lg15_encryptBlocks(
    uint8_t const * roundKeys,
    uint8_t * blocks,
    size_t numberOfBlocks
);

_Unmangled void
lg15_decryptBlocks(
    uint8_t const * roundKeys,
    uint8_t * blocks,
    size_t numberOfBlocks
);

_Unmangled void
lg15_scheduleEncryptionRoundKeys(
    uint8_t * roundKeys,
    uint8_t const * key
);

_Unmangled void
lg15_scheduleDecryptionRoundKeys(
    uint8_t * roundKeys,
    uint8_t const * key
);
