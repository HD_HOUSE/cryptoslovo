#include <err.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>
#include <assert.h>
#include "gost3411-2012-core.h"

#define READ_BUFFER_SIZE 65536



void *
memalloc(const size_t size)
{
    void *p;

    if (posix_memalign(&p, (size_t) 64, size))
        err(EX_OSERR, NULL);

    return p;
}

/// @brief хеш функция
/// @param src входной массив байт
/// @param input_len размер входного массива
/// @param hash_len режим хеш функции (256 или 512 бит)
/// @return хеш
unsigned char *__attribute__((visibility("default"))) __attribute__((used)) GetHash(unsigned char *src, int32_t input_len, int32_t hash_len)
{
    assert(hash_len == 256 || hash_len == 512);

    GOST34112012Context *CTX;
    CTX = (GOST34112012Context *)memalloc(sizeof(GOST34112012Context));

    unsigned char *digest = malloc(input_len);
    unsigned char hexdigest[129];

    unsigned char *buf __attribute__((aligned(16)));

    // инициализируем контекст
    GOST34112012Init(CTX, hash_len);

    int len = input_len > READ_BUFFER_SIZE ? READ_BUFFER_SIZE : input_len;
    buf = (unsigned char *)memalloc(len);
    memcpy(buf, src, len);

    int offset = 0;
    while (len > 0)
    {
        GOST34112012Update(CTX, buf, len);
        input_len -= len;
        offset += len;
        len = input_len > READ_BUFFER_SIZE ? READ_BUFFER_SIZE : input_len;
        memcpy(buf, src + offset, len);
    }

    // завершаем обработку
    GOST34112012Final(CTX, &digest[0]);

    // чистим память
    if (CTX != NULL)
        GOST34112012Cleanup(CTX);
    free(buf);

    return digest;
}
