Сайт приложения: https://slovo.im/ 

Лицензия: https://mit-license.org/

РЕАЛИЗАЦИЯ КРИПТОГРАФИЧЕСКИХ АЛГОРИТМОВ

* ГОСТ 34.10 - /curve && /cryptopp
* ГОСТ 34.11 - /streebog
* ГОСТ 34.12 - /cipher
* ГОСТ 34.13 - /cipher


Использовались проекты с открытым исходным кодом следующих авторов:

Copyright (c) 2013, Alexey Degtyarev <alexey@renatasystems.org> (https://github.com/adegtyarev/streebog/blob/master/LICENSE)

Сopyright InfoTeCS (https://github.com/app13y/lg15/blob/master/LICENSE.md)

libgost15.h (https://github.com/app13y/lg15/blob/master/LICENSE.md)

Использовлись отдельные файлы библиотеки Crypto++ (https://cryptopp.com/License.txt)

Хотели бы поблагодарить следующих авторов за размещение их работ в открытом доступе:

* Joan Daemen - 3way.cpp
* Leonard Janke - cast.cpp, seal.cpp
* Steve Reid - cast.cpp
* Phil Karn - des.cpp
* Andrew M. Kuchling - md2.cpp, md4.cpp
* Colin Plumb - md5.cpp
* Seal Woods - rc6.cpp
* Chris Morgan - rijndael.cpp
* Paulo Baretto - rijndael.cpp, skipjack.cpp, square.cpp
* Richard De Moliner - safer.cpp
* Matthew Skala - twofish.cpp
* Kevin Springle - camellia.cpp, shacal2.cpp, ttmac.cpp, whrlpool.cpp, ripemd.cpp
* Ronny Van Keer - sha3.cpp
* Aumasson, Neves, Wilcox-O'Hearn and Winnerlein - blake2.cpp, blake2b_simd.cpp, blake2s_simd.cpp
* Aaram Yun - aria.cpp, aria_simd.cpp
* Han Lulu, Markku-Juhani O. Saarinen - sm4.cpp sm4_simd.cpp
* Daniel J. Bernstein, Jack Lloyd - chacha.cpp, chacha_simd.cpp, chacha_avx.cpp
* Andrew Moon - donna_32.cpp, donna_64.cpp, donna_sse.cpp
