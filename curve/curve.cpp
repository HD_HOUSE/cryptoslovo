#include "curve.h"
#include <vector>
#include "../cryptopp/ecp.h"
#include "../cryptopp/osrng.h"
using namespace std;

extern "C"
{
    unsigned char *GetHash(unsigned char *, int32_t, int32_t);
}

/// @brief создает кривую с параметрами по ГОСТу
/// @return эллиптическую кривую
Curve MyCurve()
{
    CryptoPP::Integer p("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD97h");
    CryptoPP::Integer q("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C611070995AD10045841B09B761B893h");
    CryptoPP::Integer a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD94h");
    CryptoPP::Integer b("00000000000000000000000000000000000000000000000000000000000000a6h");
    CryptoPP::Integer x("0000000000000000000000000000000000000000000000000000000000000001h");
    CryptoPP::Integer y("8D91E471E0989CDA27DF505A453F2B7635294F2DDF23E3B122ACC99C9E9F1E14h");
    Curve c("kaef", p, q, a, b, x, y);
    return c;
}

std::ostream &operator<<(std::ostream &os, const std::vector<uint8_t> &v)
{
    os << "[";
    for (typename std::vector<uint8_t>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << int(*ii) << ", ";
    }
    os << "]";
    return os;
}

Curve::Curve(std::string name, CryptoPP::Integer p, CryptoPP::Integer q, CryptoPP::Integer a, CryptoPP::Integer b, CryptoPP::Integer x, CryptoPP::Integer y)
    : name(name), P(p), Q(q), A(a), B(b), X(x), Y(y), ecp(p, a, b), point(x, y)
{
}

int Curve::pointSize()
{
    return P.BitCount() > 256 ? 64 : 32;
}

CryptoPP::Integer Curve::pos(CryptoPP::Integer v)
{
    if (v < 0)
    {
        return v + P;
    }
    else
    {
        return v;
    }
}

std::string toString(const CryptoPP::Integer &a, int base)
{
    CryptoPP::Integer temp1 = a, temp2;
    static const char lower[] = "0123456789abcdef";

    const char *vec = lower;
    auto add = a.BitCount() % 8 == 0 ? 0 : 1;
    unsigned int i = a.BitCount() / (CryptoPP::SaturatingSubtract1(CryptoPP::BitPrecision(base), 1U)) + add;

    std::string s(a.BitCount() / (CryptoPP::SaturatingSubtract1(CryptoPP::BitPrecision(base), 1U)) + add, ' ');

    while (i)
    {
        CryptoPP::word digit;
        CryptoPP::Integer::Divide(digit, temp2, temp1, base);
        s[--i] = vec[digit];
        temp1.swap(temp2);
    }

    return s;
}

PublicKey::PublicKey(Curve curve, CryptoPP::ECP::Point point)
    : curve(curve), point(point)
{
}

std::string PublicKey::toHexString()
{
    std::stringstream strX;
    std::stringstream strY;
    // Записываем координаты в потоки дополняя нулями слева до 64 символов
    strX << std::setw(curve.pointSize() * 2) << std::setfill('0') << toString(point.x, 16);
    strY << std::setw(curve.pointSize() * 2) << std::setfill('0') << toString(point.y, 16);
    std::vector<uint8_t> Xs = hex2bytes(strX.str());
    std::vector<uint8_t> Ys = hex2bytes(strY.str());

    // Байты координаты Х приписываем справа к байтам координаты Y
    Xs.insert(Xs.end(), Ys.begin(), Ys.end());

    return toHex(Xs);
}

bool PublicKey::verifyDigest(vector<uint8_t> digest, vector<uint8_t> signature)
{
    // создаем строки из входных параметров, чтобы потом сделать из них числа
    string _digest = toHex(digest);
    string _signature = toHex(signature);
    int pointSize = curve.pointSize();
    if (_signature.size() != 4ull * pointSize)
    {
        throw std::invalid_argument("gost3410: len(signature) != 4*pointSize");
    }

    // создаем числа из частей подписи
    CryptoPP::Integer r((_signature.substr(0, pointSize * 2) + "h").data());
    CryptoPP::Integer s((_signature.substr(pointSize * 2) + "h").data());

    // создаем число из хеша подписанного сообщения
    CryptoPP::Integer e((_digest + "h").data());
    e %= curve.Q;
    if (e == 0)
    {
        e = 1;
    }

    auto v = e.InverseMod(curve.Q);
    auto z1 = (s * v) % curve.Q;
    auto z2 = curve.Q - ((r * v) % curve.Q);
    auto Point1 = curve.ecp.Multiply(z1, curve.point);
    auto Point2 = curve.ecp.Multiply(z2, point);
    auto lm = Point2.x - Point1.x;
    if (lm == 0)
    {
        lm += curve.P;
    }
    lm = lm.InverseMod(curve.P);
    z1 = Point2.y - Point1.y;
    lm = (lm * z1) % curve.P;
    lm = (lm * lm) % curve.P;
    lm = (lm - Point1.x - Point2.x) % curve.P;
    if (lm < 0)
    {
        lm += curve.P;
    }
    lm %= curve.P;

    // подпись валидна если выполняется это равенство
    return lm == r;
}

PrivateKey::PrivateKey(Curve curve, std::string raw)
    : curve(curve)
{
    std::vector<uint8_t> r = hex2bytes(raw);
    int pointSize = curve.pointSize();
    if (r.size() != (size_t)pointSize)
    {
        throw std::invalid_argument("size != point_size");
    }
    CryptoPP::Integer k(r.data(), r.size());
    if (k == 0)
    {
        throw std::invalid_argument("gost3410: zero private key");
    }

    key = CryptoPP::Integer(k % curve.Q);
}

std::string PrivateKey::toHexString()
{
    vector<uint8_t> bytes = hex2bytes(toString(key, 16));
    string hexKey = toHex(bytes);
    std::stringstream strS;
    strS << std::setw(curve.pointSize()*2) << std::setfill('0') << hexKey;
    return strS.str();
}

PublicKey PrivateKey::GenPublicKey()
{
    return {curve, curve.ecp.Multiply(key, curve.point)};
}

vector<uint8_t> PrivateKey::signDigest(vector<uint8_t> _digest)
{
    // создаем строку, а затем число из хеша сообщения, которое хотим подписать
    string digest = toHex(_digest);
    CryptoPP::Integer e((digest + "h").data());
    e = e % curve.Q;
    if (e == 0)
    {
        e = 1;
    }

    CryptoPP::Integer r = 0;
    CryptoPP::Integer s = 0;

    CryptoPP::AutoSeededRandomPool generator;
    while (true)
    {
        auto k = CryptoPP::Integer(generator, curve.pointSize() * 8);
        k %= curve.Q;
        if (k == 0)
        {
            continue;
        }
        auto result = curve.ecp.Multiply(k, curve.point);
        r = result.x % curve.Q;
        if (r == 0)
        {
            continue;
        }
        auto d = key * r;
        k *= e;
        s = (d + k) % curve.Q;
        if (s == 0)
        {
            continue;
        }
        break;
    }

    std::stringstream strS;
    std::stringstream strR;
    // дополняем обе компонеты подписи нулями слева до 64 символов в каждой
    strS << std::setw(64) << std::setfill('0') << toString(s, 16);
    strR << std::setw(64) << std::setfill('0') << toString(r, 16);
    // переводим результат в байты
    return hex2bytes(strR.str() + strS.str());
}

vector<uint8_t> Kek(Curve curve, PrivateKey prv, PublicKey pub, CryptoPP::Integer ukm)
{
    // умножаем публичный ключ на приватный (точку на число) с помощью арифметики эллиптических кривых
    auto point = curve.ecp.Multiply(prv.key, pub.point);
    // получившуюся точку умножаем на еще одно число (соль)
    auto key = curve.ecp.Multiply(ukm, point);

    /* Создаем публичный ключ(по сути это точка на кривой)
       Затем у получившегося объекта вызываем метод превращения в строку
       Далее берем от этой строки хеш, что в итоге и является общим ключем
    */

    PublicKey newKey(curve, key);
    unsigned char *hash = GetHash((unsigned char *)newKey.toHexString().data(), 128, 512);
    return {hash, hash + 64};
}

std::vector<uint8_t> hex2bytes(const std::string &hex)
{
    if (hex.size() % 2)
    {
        throw invalid_argument("hex.size() % 2 == 1");
    }
    std::vector<uint8_t> v;

    for (unsigned int i = 0; i < hex.length(); i += 2)
    {
        std::string byteString = hex.substr(i, 2);
        uint8_t byte = (uint8_t)strtol(byteString.c_str(), NULL, 16);
        v.push_back(byte);
    }
    return v;
}

std::string toHex(const std::vector<uint8_t> &v)
{
    std::stringstream ss;
    ss << std::hex << std::setfill('0');
    std::vector<uint8_t>::const_iterator it;

    for (it = v.begin(); it != v.end(); it++)
    {
        ss << std::setw(2) << static_cast<unsigned>(*it);
    }

    return ss.str();
}
